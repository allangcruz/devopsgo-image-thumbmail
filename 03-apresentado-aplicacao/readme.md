# Tratamento de image Thumbnail

Thumbnail é uma versão em miniatura de imagens usadas na Internet para facilitar as buscas. O nome em inglês significa “unha do polegar”, indicando algo pequeno.

Mesmo que a maioria das pessoas não saiba o que é uma thumbnail, é bem provável que já tenham se deparado com várias ao longo da vida.

Sabe aquela imagem parada que aparece em um vídeo do YouTube? Ou, talvez, você já viu as fotos em um tamanho reduzido na galeria do seu celular?

Caso contrário, certamente já as observou no seu blog favorito e, após clicá-la, você pôde testemunhá-la no tamanho original.
