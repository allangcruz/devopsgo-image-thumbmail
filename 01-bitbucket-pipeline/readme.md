# Bitbucket Pipeline

O Bitbucket Pipelines é um serviço integrado de CI/CD, que permite criar, testar e implantar código automaticamente.
Ele está embutido no Bitbucket, e você pode configurá-lo através de um arquivo yaml criado na raiz do repositório.

Antes de usar o serviço do Bitbucket Pipeline é necessário habilitar, conforme a imagem abaixo.

![enable-bitbucket](imgs/enable-pipeline.png "Enable pipeline.")


# Sobre custo e preço.

O serviço do bitbucket descreve os custo da utilização da ferramenta nos links abaixo:

* [https://bitbucket.org/product/pricing?tab=host-in-the-cloud](https://bitbucket.org/product/pricing?tab=host-in-the-cloud)
* [https://bitbucket.org/product/features/pipelines](https://bitbucket.org/product/features/pipelines)

### Configurar básica arquivo bitbucket-pipelines.yml

Com uma configuração básica, você pode fazer coisas como escrever scripts para criar e implantar seus projetos e configuração de caches para acelerar `build`.
Você também pode especificar `container` diferentes para cada etapa para gerenciar dependências diferentes nas ações que você está executando no seu pipeline.

Abaixo a imagem descreve a estrutura básica da pipeline:

![pipeline-structure](imgs/pipeline-structure.png "Pipeline Structure.")

### Hello World com a pipeline

```yaml
image: golang:1.12
pipelines:
  default:
    - step:
        script:
          - echo "Inicianlizando pipeline ... "
          - go version
          - echo "Finalizando pipeline ... "
```

### Exemplo com varios steps

```yaml
image: golang:1.12
pipelines:
  default:
    - step:
        name: Build
        script:
          - echo "Inicianlizando build ... "
          - go version
          - echo "Finalizando build ... "

    - step:
        name: Test
        script:
          - echo "Inicianlizando test ... "
          - echo "Finalizando test ... "

    - step:
        name: Deploy
        script:
          - echo "Inicianlizando deploy ... "
          - echo "Finalizando deploy ... "
```

### Exemplo com várias branchs

```
image: golang:1.12
pipelines:
  branches:
    dev:
      - step:
          name: Etapa 1
          script:
            - echo "Inicianlizando build ... "
            - echo "Finalizando build ... "

    staging:
      - step:
          name: Etapa 1
          script:
            - echo "Inicianlizando build ... "
            - echo "Finalizando build ... "

    master:
      - step:
          name: Etapa 1
          script:
            - echo "Inicianlizando build ... "
            - echo "Finalizando build ... "   
```

### Exemplo com step customizados e referenciando branch

```
pipelines:
  custom:
    test:
    - step: &test
        name: Test
        image:
          name: golang:1.12
        script:
          - echo "Executando setp de Teste"
    build:
    - step: &build
        name: Build
        image:
          name: golang:1.12
        script:
          - echo "Executando setp de Build"
    deploy:
    - step:
        <<: *build
    - step: &deploy
        name: Deploy
        image:
          name: golang:1.12
        script:
          - echo "Executando setp de Deploy"
  branches:
    master:
    - step:
        <<: *test
    - step:
        <<: *build
    - step:
        name: Deploy
        deployment: production
        trigger: manual
        <<: *deploy
    dev:
    - step:
        <<: *test
    - step:
        <<: *build
    - step:
        deployment: staging
        <<: *deploy
```

### Exemplo com step manual de confirmação

```
image: golang:1.12
pipelines:
  branches:
    master:
    - step:
        script:
        - echo "Executando setup de Test"
    - step:
        script:
        - echo "Executando setup de Build"
    - step:
        name: Deploy
        deployment: production
        trigger: manual
        script:
        - echo "Executando setup de Deploy"

```

### Exemplo com cache

```
pipelines:
  custom:
    deploy:
    - step:
        <<: *build
    - step: &deploy
        name: Etapa 1
        image:
          name: golang:1.12
        caches:
          - pkgmod
        script:
          - echo "Executando setup de Build"
  branches:
    master:
    - step:
        name: Deploy
        deployment: production
        trigger: manual
        <<: *deploy

definitions:
  caches:
    pkgmod: /go/pkg/mod
```

### Exemplo com artefatos

```
pipelines:
  custom:
    build:
    - step: &build
        name: Build
        image:
          name: golang:1.12
        caches:
          - pkgmod
        script:
          - echo "Executando setup de Build"
        artifacts:
          - bin/**
    deploy:
    - step:
        <<: *build
    - step: &deploy
        name: Etapa 1
        image:
          name: golang:1.12
        caches:
          - pkgmod
        script:
          - echo "Executando setup de Deploy"
  branches:
    master:
    - step:
        name: Deploy
        deployment: production
        trigger: manual
        <<: *deploy

definitions:
  caches:
    pkgmod: /go/pkg/mod
```

# Lista de comandos do bitbucket-pipeline

[https://confluence.atlassian.com/bitbucket/configure-bitbucket-pipelines-yml-792298910.html](https://confluence.atlassian.com/bitbucket/configure-bitbucket-pipelines-yml-792298910.html)

#### Referência

* [https://confluence.atlassian.com/bitbucket/get-started-with-bitbucket-pipelines-792298921.html](https://confluence.atlassian.com/bitbucket/get-started-with-bitbucket-pipelines-792298921.html)
* [https://confluence.atlassian.com/bitbucket/build-test-and-deploy-with-pipelines-792496469.html](https://confluence.atlassian.com/bitbucket/build-test-and-deploy-with-pipelines-792496469.html)
* [https://confluence.atlassian.com/bitbucket/configure-bitbucket-pipelines-yml-792298910.html](https://confluence.atlassian.com/bitbucket/configure-bitbucket-pipelines-yml-792298910.html)
* [https://confluence.atlassian.com/bitbucket/variables-in-pipelines-794502608.html](https://confluence.atlassian.com/bitbucket/variables-in-pipelines-794502608.html)
* [https://bitbucket.org/product/br/pricing](https://bitbucket.org/product/br/pricing)
* [https://bitbucket.org/product/features/pipelines](https://bitbucket.org/product/features/pipelines)
* [https://confluence.atlassian.com/bitbucket/configuring-your-pipeline-872013574.html](https://confluence.atlassian.com/bitbucket/configuring-your-pipeline-872013574.html)