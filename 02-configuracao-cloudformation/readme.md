# Configurando infra como código

O arquivo `devopsgo_cfn.zip` contém os scripts necessarios para crias as depedências na AWS, sendo elas:

* S3
* SQS
* SNS
* IAM
* Lambda
* Cloud Formation
* SSM
* KMS
* Cloud Watch
* Api Gateway

Será necessario configurar algumas variáveis de ambiente no recurso de `Deployment` do Bitbucket Pipeline.

* **AWS_ACCESS_KEY_ID**: Sua Key Id gerada pelo IAM
* **AWS_SECRET_ACCESS_KEY**: Sua chave secret gerada pelo IAM
* **AWS_REGION**:  us-east-1
* **AWS_ACCOUNT_ID**: Seu identificador
* **ENVIRONMENT**: dev, staging, prod
* **PROJECT_NAME**: nome do seu projeto
* **SQUAD_NAME**: nome do time do squad