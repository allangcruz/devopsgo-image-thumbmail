# Treinamento DevOpsGO

![devopsgo](devops-go.png "DevOpsGo.")

O objetivo desse treinamento é apresentar o Bitbucket Pipeline aplicando o CI e CD.

## Cenário

O Product Owner coletou informações de uma nova funcionalidade para tratar miniaturas das imagens, recurso conhecido como **Thumbnail**.
Foi criado a história de usuário para o time de desenvolvimento atuar.

## Pipeline de Implantação

Em resumo é a automação de atividades que leva o código produzido pelo desenvolvedor até aos ambiente de entrega sendo eles, `dev`, `teste` ou `produção`.

**Dentro dessas atividades, são envolvidos os processos**:
* CI - Integração Contínua (Continuous Intregration)
* CD - Entrega Contínua/Implantação Contínua (Continuous Delivery/Continuous Deployment)

**Objetivo do pipeline de implantação**

 - Automatizar a entrega de código em produção de forma consistente, segura e rápida.
 - Deixar evidente todo o processo de entrega de software nos ambientes pré configurados.

**Integração Contínua (Continuous Integration)**

 - Baseado em práticas do XP (Extreme Programming) em que encoraja desenvolvedores a integrar trexos de códigos o mais frequentemente possível para que os problemas sejam detectados e sanados rapidamente, caso ocorram.
 - Pre-requisitos: Controle de versão, processo automatizado de compilação e aceitação da equipe.

**Entrega Contínua (Continuous Delivery)**

 - Sendo uma extensão da integração contínua (Continuous Integration), garante a liberação de novas alterações de código aos clientes de forma sustentável, ou seja, além de automaticar seus testes, automatiza o processo de liberação que poderá ser implantado na produção em qualquer momento com um clique de um botão, para este caso é necessária a intervenção humana. 
 - Garante que o código e a infraestrutura como código sempre estejam prontas para implantação de uma forma segura na produção.

**Implantação Contínua (Continuous Deployment)**

 - É bem parecido com a Entrega Contínua (Continuous Delivery), só que aqui as liberaçãos para a produção ocorrem automaticamente. Existe maturidade para que isso ocorra?

## Roteiro

* [00-configuracao-ambiente](https://bitbucket.org/allangcruz/devopsgo-image-thumbmail/src/master/00-configuracao-ambiente/readme.md)
* [01-bitbucket-pipeline](https://bitbucket.org/allangcruz/devopsgo-image-thumbmail/src/master/01-bitbucket-pipeline/readme.md)
* [02-configuracao-cloudformation](https://bitbucket.org/allangcruz/devopsgo-image-thumbmail/src/master/02-configuracao-cloudformation/readme.md)
* [03-apresentado-aplicacao](https://bitbucket.org/allangcruz/devopsgo-image-thumbmail/src/master/03-apresentado-aplicacao/readme.md)

### Contribuidores

* GitHub
    * [Allan Gonçalves da Cruz](https://github.com/allangcruz)
    * [Rafael Sobreira Braga](https://github.com/allangcruz)

* Linkedin
    * [Allan Gonçalves da Cruz](https://www.linkedin.com/in/allan-gon%C3%A7alves-da-cruz-b7a24341)
    * [Rafael Sobreira Braga](https://www.linkedin.com/in/rafaelsobreirabraga)