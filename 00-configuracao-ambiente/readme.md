# Configurando ambiente do Git com o Bitbucket e Amazon Web Service (AWS).

Essa etapa descreve detalhadamente as configurações de Bitbucket e repositório do projeto.

### 1. Criação da conta no Bitbucket em [https://bitbucket.org](https://bitbucket.org).

### 2. Configurando chave SSH para enviar e puxar commits do repositório.

É necessário que o software `git` esteja instalado em sua máquina, caso nao esteja segue o link do website para a instalação [https://git-scm.com/downloads](https://git-scm.com/downloads).

Após a instalação crie uma nova chave ssh ou utilize a sua existente.

```
cd ~/.ssh/
ssh-keygen -t rsa -b 4096 -C "seu_email@example.com"
```

Após executar o comando acima será solicitado algumas perguntas conforme abaixo:

```
Generating public/private rsa key pair.
Enter file in which to save the key (/home/USUARIO/.ssh/id_rsa): id_rsa_NOME-DO-ALUNO_bitbucket
Enter passphrase (empty for no passphrase): NÃO COLOQUE SENHA, APERTE ENTER
Enter same passphrase again: NÃO COLOQUE SENHA, APERTE ENTER
Your identification has been saved in id_rsa_NOME-DO-ALUNO_bitbucket.
Your public key has been saved in id_rsa_NOME-DO-ALUNO_bitbucket.pub.
The key fingerprint is:
SHA256:N9afUtUfhPnSkmx6wquj0Kp2EK5zfHjq0ANrX5W2R0Q seu_email@example.com
The key's randomart image is:
+---[RSA 4096]----+
|         E    o. |
|        .    o. .|
|         .  . +.o|
|   .    o  . * +o|
| .. .  +S.= + + .|
|  +o  + oo = + . |
| +oooo o .  = o  |
|.oo=o+o .. . .   |
|  ==*. ...o      |
+----[SHA256]-----+
```

Adicionar sua chave SSH ao ssh-agent

```
eval "$(ssh-agent -s)"
```

Adicione sua chave SSH privada ao ssh-agent. 

```
ssh-add ~/.ssh/id_rsa_NOME-DO-ALUNO_bitbucket
```

Mapeando referência de key.

```
Host bitbucket.pessoal
  HostName bitbucket.org
  User git
  IdentitiesOnly yes
  IdentityFile ~/.ssh/id_rsa_NOME-DO-ALUNO_bitbucket
```

Adicionar a chave pública no bitbucket.

```
 cat ~/.ssh/id_rsa_NOME-DO-ALUNO_bitbucket.pub
```

Copiar o texto imprimido pelo comando `cat` e adicionar no bitbucket na sessão de configuração:

![new_key_ssh](imgs/ssh-add-new-key.png "Adicionando nova chave ssh.")

Apos clicar em `Add key` preencha os campos e clique em `Add key`:

![new_key_ssh](imgs/add-new-key.png "Criar nova chave ssh.")


# Configurando repositório

Criar dois repositórios, um para conter a infra como código em Cloud Formation, que se chamará `devopsgo-cfn`. 

O segundo repositório conterá nosso projeto que aplicaremos também a integração continua, o nome do repositório será `devopsgo-image-thumbmail` .

### Download do projeto
* [cloudformation](https://bitbucket.org/allangcruz/devopsgo-image-thumbmail/src/master/02-configuracao-cloudformation)
* [aplicacao](https://bitbucket.org/allangcruz/devopsgo-image-thumbmail/src/master/03-apresentado-aplicacao)

Após criar os dois repositórios iremos configurar o **remote** para referéncia nosso código em nossa máquina com o repositório criado anteriormente.
Execute o comando:

```
git remote add origin <url-do-repositorio>
```

Criar as branch do ciclo de vida do projeto sendo elas `dev`, `staging` e `master`. Configurar a branch **dev** com padrão.


### Configurando Bitbucket Pipeline

[bitbucket-pipeline](https://bitbucket.org/allangcruz/devopsgo-image-thumbmail/src/master/01-bitbucket-pipeline/readme.md)


# Criação da Conta AWS

Amazon Web Services também conhecido como AWS, é uma plataforma de serviços de computação em nuvem, que formam uma plataforma de computação na nuvem oferecida pela amazon.com.

Para criar uma nova conta da AWS é necessário informar um cartão de crédito válido.

Seque abaixo o exemplo das informações solicitadas para criar uma nova conta:

- **Primeira Etapa**

![imagem-001](imgs/001-aws.png "imagem-001.")

- **Segunda Etapa**

![imagem-002](imgs/002-aws.png "imagem-002.")

- **Terceira Etapa**

![imagem-003](imgs/003-aws.png "imagem-003.")

- **Quarta Etapa**

![imagem-004](imgs/004-aws.png "imagem-004.")

- **Quinta Etapa**

![imagem-005](imgs/005-aws.png "imagem-005.")

### Criação de chave de acesso do IAM.

O serviço da AWS IAM é responsável por gerenciar permissão de todos os recursos da AWS.

Segue abaixo de como criar uma nova chaves para acessar os recursos:

- **Primeira Etapa**

![iam-001](imgs/001-iam.png "iam-001.")

- **Segunda Etapa**

![iam-002](imgs/002-iam.png "iam-002.")

- **Terceira Etapa**

![iam-003](imgs/003-iam.png "iam-003.")

- **Quarta Etapa**

![iam-004](imgs/004-iam.png "iam-004.")

- **Quinta Etapa**

![iam-005](imgs/005-iam.png "iam-005.")

- **Sexta Etapa**

![iam-006](imgs/006-iam.png "iam-006.")


